
#include <iostream>
#include <cmath>


class Vector
{
private:
		
	double x;
	double y;
	double z;

public:

	Vector() : x(9), y(6), z(7)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		std::cout << "\n" << x << " " << y << " " << z << "\n";
	}

public:

	double Lenght()
	{
		double Sum = (x * x) + (y * y) + (z * z);
		return sqrt(Sum);
	}

};

int main()
{
	Vector V;
	V.Show();
	V.Lenght();
	std::cout << V.Lenght();

	return 0;
}

